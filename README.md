# top-langs-gitea
Generate an image showing top languages for Gitea/Codeberg accounts

## Example
`./top-langs-gitea.lua codeberg.org/nazrin img.webp -R st,dmenu`

![image](./img.webp)

## Requires
* [Luvit](https://luvit.io/ "Luvit")
* * `curl -L https://github.com/luvit/lit/raw/master/get-lit.sh | sh`
* [Argparse](https://luarocks.org/modules/argparse/argparse "link")
* * `luarocks install --lua-version=5.1 argparse`
* [LIL](https://luarocks.org/modules/nazrin/lil "link")
* * `luarocks install --lua-version=5.1 lil`

## --help
```css
Usage: top-langs-gitea.lua [-h] [--reset-cache] [--skip-header]
       [--skip-bytes] [--absolute] [--bg <bg>] [--fg <fg>]
       [--per <per>] [--font-top <font_top>] [--font-name <font_name>]
       [--font-per <font_per>] [--font-lang <font_lang>]
       [--font-bytes <font_bytes>] [--font-top-size <font_top_size>]
       [--font-name-size <font_name_size>]
       [--font-per-size <font_per_size>]
       [--font-lang-size <font_lang_size>]
       [--font-bytes-size <font_bytes_size>] [--top <top>]
       [--max <max>] [--name <name>] [--skip-repos <skip_repos>]
       [--skip-langs <skip_langs>] <id> <path>

Generate an image showing top languages for Gitea accounts

Arguments:
   id                    Domain/Username
   path                  Output image

Options:
   -h, --help            Show this help message and exit.
   --reset-cache, -C     Overwrites files in cache
   --skip-header, -H     Don't include name and icon
   --skip-bytes, -B      Don't show bytes per lang
   --absolute, -a        Use percentages for all langs instead of only included ones
   --bg <bg>,            Background colour (default: fff)
     -b <bg>
   --fg <fg>,            Text colour (default: 000)
     -f <fg>
   --per <per>,          Percentage format (default: %.1f%%)
      -p <per>
   --font-top <font_top> Top lang font (default: sans)
   --font-name <font_name>
                         Name font (default: sans:bold)
   --font-per <font_per> Percentage font (default: sans)
   --font-lang <font_lang>
                         Lang font (default: sans)
   --font-bytes <font_bytes>
                         Lang font (default: sans)
   --font-top-size <font_top_size>
                         Top lang font size (default: 20)
   --font-name-size <font_name_size>
                         Name font size (default: 20)
   --font-per-size <font_per_size>
                         Percentage font size (default: 20)
   --font-lang-size <font_lang_size>
                         Lang font size (default: 20)
   --font-bytes-size <font_bytes_size>
                         Lang font size (default: 20)
   --top <top>,          Top lang text (default: Top languages)
      -t <top>
   --max <max>,          Max langs (default: 8)
      -m <max>
   --name <name>,        Name format (default: {domain}/{user})
       -t <name>
   --skip-repos <skip_repos>,
             -R <skip_repos>
                         Repos to skip
   --skip-langs <skip_langs>,
             -L <skip_langs>
                         Langs to skip
```

## Todo

* Make the rendering more generic, less magic numbers, resizable, columns
* Clean up some of the logic

