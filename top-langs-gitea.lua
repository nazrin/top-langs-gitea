#!/usr/bin/env luvit
local argparse = require("argparse")
local https = require("https")
local json = require("json")
local fs = require("fs")
local lil = require("lil")
local languageColours = require("top-langs-gitea-colours")

local function formatBytes(n, f)
	local u = { "b", "kb", "mb", "gb", "tb" }
	local b = 1
	while b < #u do
		if n >= 1024 then
			n = n / 1024
			b = b + 1
		else
			break
		end
	end
	local s = n == math.floor(n) and ("%d"):format(n) or ("%0.2f"):format(n)
	return (f or "%s%s"):format(s, u[b])
end
local function split(self, d)
	local arr = {}
	for m in self:gmatch("[^"..d.."]+") do
		table.insert(arr, m)
	end
	return arr
end
local function getTmpDir()
	local root
	if jit.os == "Windows" then
		root = os.tmpname():gsub("[^/\\]+$", "")
	else
		root = "/tmp/"
	end
	return root .. "/mstusedlangsscache/"
end
fs.mkdirSync(getTmpDir())

local params
do
	local function commaSeperate(p)
		local t = {}
		for _,x in ipairs(split(p, ",")) do
			t[x] = true
		end
		return t
	end
	local parser = argparse("top-langs-gitea.lua", "Generate an image showing top languages for Gitea accounts")
	parser:flag("--reset-cache -C", "Overwrites files in cache")
	parser:flag("--skip-header -H", "Don't include name and icon")
	parser:flag("--skip-bytes -B",  "Don't show bytes per lang")
	parser:flag("--absolute -a",    "Use percentages for all langs instead of only included ones")
	parser:argument("id",   "Domain/Username")
	parser:argument("path", "Output image")
	parser:option("--bg -b",           "Background colour",    "fff",             nil)
	parser:option("--fg -f",           "Text colour",          "000",             nil)
	parser:option("--per -p",          "Percentage format",    "%.1f%%",          nil)
	parser:option("--font-top",        "Top lang font",        "sans",            nil)
	parser:option("--font-name",       "Name font",            "sans:bold",       nil)
	parser:option("--font-per",        "Percentage font",      "sans",            nil)
	parser:option("--font-lang",       "Lang font",            "sans",            nil)
	parser:option("--font-bytes",      "Lang font",            "sans",            nil)
	parser:option("--font-top-size",   "Top lang font size",   "20",              nil)
	parser:option("--font-name-size",  "Name font size",       "20",              nil)
	parser:option("--font-per-size",   "Percentage font size", "20",              nil)
	parser:option("--font-lang-size",  "Lang font size",       "20",              nil)
	parser:option("--font-bytes-size", "Lang font size",       "20",              nil)
	parser:option("--top -t",          "Top lang text",        "Top languages",   nil)
	parser:option("--max -m",          "Max langs",            "8",               tonumber)
	parser:option("--name -t",         "Name format",          "{domain}/{user}", nil)
	parser:option("--skip-repos -R",   "Repos to skip",        {},                commaSeperate)
	parser:option("--skip-langs -L",   "Langs to skip",        {},                commaSeperate)
	local arg = {}
	for i,a in ipairs(args) do
		arg[i - 1] = a
	end
	params = parser:parse(arg)
	params.domain, params.account = params.id:match("^([^/]+)/([^/]+)")
	if not (params.domain and params.account) then parser:error("Invalid id format") end
end

local function formatVariables(fmt, user, stats)
	return fmt
		:gsub("{domain}", params.domain)
		:gsub("{user}", user.username)
		:gsub("{totalBytes}", stats.totalBytes)
end

local function generateStats(bytesPerLang, repos)
	local langs = {}
	local totalBytes = 0
	for lang,bytes in pairs(bytesPerLang) do
		table.insert(langs, { name = lang, bytes = bytes, repos = {} })
	end
	table.sort(langs, function(a, b)
		return a.bytes > b.bytes
	end)
	for l,lang in ipairs(langs) do
		totalBytes = totalBytes + lang.bytes
	end
	langs.totalBytes = formatBytes(totalBytes)
	if #langs > params.max then
		for i=params.max+1,#langs do
			langs[i] = nil
		end
	end
	for l,lang in ipairs(langs) do
		local f = lang.bytes / totalBytes
		lang.percentage = params.per:format(f * 100)
		lang.fraction = f
		lang.colour = languageColours[lang.name] or "000"
		for repo,langs in pairs(repos) do
			if langs[lang.name] then
				table.insert(lang.repos, repo)
			end
		end
	end
	if params.absolute then
		for r=#langs,1,-1 do
			local lang = langs[r]
			if params.skip_langs[lang.name] then
				table.remove(langs, r)
			end
		end
	end
	return langs
end

local function getGridPos(i)
	return 40, (i * 60) + 100
end

local function generateImage(stats, user, avatarData)
	local img = lil.new(500, #stats * 60 + 180):fill(params.bg)
	if avatarData then
		local avatar = lil.import(avatarData):resize(80, 80)
		img:comp(avatar, 40, 40)
	end
	if user then
		local font
		-- Top
		font = lil.font(params.font_top, params.fg, params.font_top_size)
		img:text(font, formatVariables(params.top, user, stats), 130, 50)
		-- Name
		font = lil.font(params.font_name, params.fg, params.font_name_size)
		img:text(font, formatVariables(params.name, user, stats), 130, 70)
		-- Bytes
		if not params.skip_bytes then
			font = lil.font(params.font_bytes, params.fg, params.font_bytes_size)
			local t = font:text(stats.totalBytes)
			img:comp(t, img.w - t.w - (40), 50)
		end
	end
	-- Langs
	local langFont = lil.font(params.font_lang, params.fg, params.font_lang_size)
	local perFont  = lil.font(params.font_per, params.fg, params.font_per_size)
	for l,lang in ipairs(stats) do
		local canvas = lil.new(500 - 80, 100)
		do
			local te = lang.percentage
			if not params.skip_bytes then
				te = ("%s %s"):format(formatBytes(lang.bytes), te)
			end
			local t = perFont:text(te)
			canvas:comp(t, canvas.w - t.w, 0)
		end
		canvas:text(langFont, lang.name, 0, 0)
		local function bar(img, colour, per, x, y, w, h)
			local c = lil.new(w, h)
			c:circle("fff", h/2, h/2, h/2+1, h/2+1)
			c:rect("fff", h/2, 0, w-h, h)
			c:circle("fff", w-h/2-1, h/2, h/2+1, h/2+1)
			if w * per >= 1 then
				c:rect(colour, 0, 0, w * per, h, { blend = "and" })
			end
			c:rect("ddd", w * per, 0, w, h, { blend = "and" })
			img:comp(c, x, y)
		end
		bar(canvas, lang.colour, lang.fraction, 5, 5 + 20, canvas.w-10, 14)
		local x, y = getGridPos(l)
		img:comp(canvas, x, y)
	end
	if not user then
		img:crop(0, 120, img.w, img.h - 120)
	end
	img:save(params.path)
end

local function cacheFormat(url)
	return ("%s/%s"):format(getTmpDir(), url:gsub("https?://", ""):gsub("[/]", "_"))
end

local function get(url, cb, opts)
	opts = opts or {}
	local cachePath = cacheFormat(url)
	local function process(data)
		if opts.json == false then
			return data
		else
			return json.parse(data)
		end
	end
	if not params.reset_cache and fs.existsSync(cachePath) then
		local data = io.open(cachePath):read("a")
		local j = process(data)
		cb(j)
	else
		https.get(url, function(res)
			local chunks = {}
			res:on("data", function(ch)
				table.insert(chunks, ch)
			end)
			res:on("end", function()
				local data = table.concat(chunks)
				local j = process(data)
				if res.statusCode ~= 200 then
					print("Got error", res.statusCode)
					os.exit(1)
				end
				fs.writeFileSync(cachePath, data)
				cb(j)
			end)
			res:on("error", function(err)
				print(err)
				os.exit(1)
			end)
		end)
		os.execute("sleep 0.2") -- Slight selfthrottling to be nice :)
	end
end

get(("https://%s/api/v1/users/%s/repos?page=1&limit=0"):format(params.domain, params.account), function(repos)
	if #repos < 1 then
		print(("No repos for user %s/%s"):format(params.domain, params.account))
		os.exit(1)
	end
	for r=#repos,1,-1 do
		local repo = repos[r]
		if params.skip_repos[repo.name] then
			table.remove(repos, r)
		end
	end
	local done = 0
	local bytesPerLang = {}
	local langsPerRepo = {}
	for r,repo in ipairs(repos) do
		get(("https://%s/api/v1/repos/%s/%s/languages"):format(params.domain, params.account, repo.name), function(repoLangs)
			langsPerRepo[repo.name] = repoLangs
			for lang,bytes in pairs(repoLangs) do
				if not (params.skip_langs[lang] and not params.absolute) then
					if not bytesPerLang[lang] then
						bytesPerLang[lang] = 0
					end
					bytesPerLang[lang] = bytesPerLang[lang] + bytes
				end
			end
			done = done + 1
			if done == #repos then
				local stats = generateStats(bytesPerLang, langsPerRepo)
				if params.skip_header then
					generateImage(stats)
				else
					get(repo.owner.avatar_url, function(avatar)
						generateImage(stats, repo.owner, avatar)
					end, {json = false})
				end
			end
		end)
	end
end)

